---
title: "Storing and retrieving data"
cover-img: /assets/img/pexels-tembela-bohle-1884581.jpg
thumbnail-img: /assets/img/pexels-oleg-magni-1005638.jpg
share-img: /assets/img/pexels-tembela-bohle-1884581.jpg
author: martin
---

In yesterday's post we learnt how to create a basic plugin that would change the
OPAC pink once installed.

It was a great example to get us up and running, but it lacked any form of 
user configuration.. what if they wanted a green OPAC instead? Today we will 
remedy that by adding a basic configuration page and using the **store_data** 
and **retrieve_data** methods to maintain our settings.

Lets add a configuration page to our plugin.  Koha uses Template Toolkit for 
its pages and plugins are no different.  You can even use core includes in your
templates to maintain the same look as the rest of Koha.  Lets add our template
asset within a directory next to our base class: `/FancyPlugin/configure.tt`

Lets start the file with some boilerplate: The following will add the standard 
Koha `<head>`, set the title of the page to conform with tradition and add a 
breadcrumb as well as including the opening `<div>` for the main content.

```html
[% INCLUDE 'doc-head-open.inc' %]
    <title>Koha: FancyPlugin: Configuration</title>
[% INCLUDE 'doc-head-close.inc' %]
</head>

<body>
    [% INCLUDE 'header.inc' %]
    [% INCLUDE 'cat-search.inc' %]

    <div id="breadcrumbs">
        <a href="/cgi-bin/koha/mainpage.pl">Home</a>
        &rsaquo; <a href="/cgi-bin/koha/plugins/plugins-home.pl">Plugins</a>
        &rsaquo; Fancy Plugin
        &rsaquo; Configuration
    </div>
    
    <div class="main container-fluid">
```

Now we can add the plugin specific content. Koha has a fairly consistent page
layout using the bootstrap CSS frame work, and we can emulate that to make our
plugins configuration page look polished.

Let's add a placeholder for a logo:

```html
        <div class="col-sm-2">
            <aside>
                <img src="https://gitlab.com/koha-community/Koha/-/raw/master/koha-tmpl/intranet-tmpl/prog/img/koha-logo.png?inline=false" />
            </aside>
        </div>
```

Finally, lets add a form and complete the template:

```html
        <div class="col-sm-10 col-sm-push-2">
            <main>
        
                <h3>Koha: Fancy plugin: Configuration</h3>
                    
                <!-- Notice our form here has no 'action', this is good, it means that our forms will always get passed back to 'plugins/run.pl'. You could hard code it instead if you prefer -->
                <form method="get">
                    <!-- Always pass these two parameters so the plugin system knows what to execute! -->
                    <input type="hidden" name="class" value="[% CLASS | html %]"/>
                    <input type="hidden" name="method" value="[% METHOD | html %]"/>
        
                    <fieldset class="rows">
                        <legend>Color Settings</legend>
        
                        <ol>
                            <li>
                                <label for="color">Color: </label>
                                <select name="color">
                                [% FOREACH option IN options %]
                                    [% IF option == color %]
                                    <option value="[% option %]" selected="selected">[% option %]</option>
                                    [% ELSE %]
                                    <option value="[% option | html %]">[% option | html %]</option>
                                    [% END %]
                                [% END %]
                                </select>
                            </li>
                        </ol>
                    </fieldset>
        
                    <fieldset class="action">
                         <input type="hidden" name="save" value="1" />
                         <input type="submit" value="Save configuration" />
                         <a class="cancel" href="/cgi-bin/koha/plugins/plugins-home.pl">Cancel</a>
                    </fieldset>
                </form>
            </main>
        </div>

    </div>

    [% INCLUDE 'intranet-bottom.inc' %]
```

Now we have a template, but we don't yet have any code behind it.  We need to 
add a method to our class to display this template and handle the storage of
data. 

In this case, we'll use the `configure` hook. If a plugin is found to contain 
such a method then a link to your configuration page will be presented to the 
user on the plugins management page.

```perl
sub configure {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    unless ( $cgi->param('save') ) {
        my $template = $self->get_template({ file => 'configure.tt' });

        ## Define a list of color options
        $template->param(
            options => [ 'pink', 'red', 'orange', 'blue' ],
        );

        ## Grab the values we already have for our settings, if any exist
        $template->param(
            color => $self->retrieve_data('color'),
        );

        $self->output_html( $template->output() );
    }
    else {
        $self->store_data(
            {
                color => scalar $cgi->param('color'),
            }
        );
        $self->go_home();
    }
}
```

Our configure method includes a few more methods inherited from our base class. 
Firstly, we can see that there is a reference to the global cgi object from 
`$self`. This allows us to use cgi methods to access the form parameters. 

We also use the `retrieve_data` and `store_data` methods to handle saving and 
referencing our configuration.  These methods allow us to store data in the koha 
database in a table that's unique to our plugin.

Finally, we should update the hook used in our first post to utilise the new 
configuration.

```perl
sub opac_head {
    my ( $self ) = @_;

    my $color = $self->retrieve_data('color');
    return qq{
      <style>
        body {background-color: $color;}
      </style>
    };
}
```
