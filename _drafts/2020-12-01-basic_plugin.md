---
title: "1st"
bg: blue
color: white
fa-icon: fa-plug
---

# Koha plugins?

Koha has a cool way to add funcitonality: a plugin system.

Plugins are used by Koha in different ways:

* Using Koha plugin **hooks** in specific places
* Implementing API routes that are injected into Koha's core API extending it

## KitchenSink, the reference

The main source for references on writing plugins, is the [Kitchen sink plugin](https://github.com/bywatersolutions/koha-plugin-kitchen-sink). It usually implements all the things that are available in Koha.

## What makes a Zip file a Koha plugin?

Plugins are bundled files in the form of a Zip file, using the _.kpz_ extension. There needs to be
a base class, that inherits from _Koha::Plugins::Base_ like this:

```perl
package Koha::Plugin::FancyPlugin;

use Modern::Perl;

use base qw(Koha::Plugins::Base);

our $VERSION = "{VERSION}";

our $metadata = {
    name            => 'Our fancy plugin',
    author          => 'Your name',
    description     => 'Some useful description, think of end users!',
    date_authored   => '2020-12-01',
    date_updated    => "{DATE_UPDATED}",
    minimum_version => '19.1100000',
    maximum_version => undef,
    version         => $VERSION,
};

sub new {
    my ( $class, $args ) = @_;

    $args->{'metadata'} = $metadata;
    my $self = $class->SUPER::new($args);

    return $self;
}
```

This example is the minimum you need to have in your plugin class for it to be picked as a plugin
in Koha.

The only thing the __new()__ constructor does is adding the _metadata_ into its internal structure.
(yes, I know: we should make it automatically). In some plugins you might need some more initialization
and that's why the constructor is there... but... let's move on.

The _metadata_ structure is pretty straight-forward to read. The only attributes that have some semantics
are *minimum_version* and *maximum_version*, which refer to the Koha version the plugin is expected to 
work on, and _version_. The latter is used when installing/updating the plugin for comparing with the
currently installed version.

You will notice some placeholders, *{VERSION}* and *{DATE_UPDATED}*. Those are usually replaced by the
building tools. You can safelly hardcode values for them.

## How do we build a plugin?

###  Straight-forward approach
As a start, you can just create a Zip file including your files:

```shell
$ zip -r koha-plugin-fancy-v0.0.1.kpz .
```

This will produce a _.kpz_ file that is suitable for installing in Koha.

### Advanced method using Gulp (recommended)

The KitchenSink plugin (and many others) include a so-called [gulpfile](https://github.com/bywatersolutions/dev-koha-plugin-kitchen-sink/blob/master/gulpfile.js) in the project root directory. This file can be used 
and adapted for your own plugin's needs. The one shipped with the [Kitchen sink plugin](https://github.com/bywatersolutions/koha-plugin-kitchen-sink) is written to work with Gulp v4, in Node.js v12. Those tools are
shipped with [KTD](https://gitlab.com/koha-community/koha-testing-docker) and [KohaDevBox](https://gitlab.com/koha-community/kohadevbox) which are the development environment we use.

With such a _gulpfile_, the process of packaging the plugin requires to run:

```shell
$ gulp build
```
If you borrow the _gulpfile.js_ from another project, take a look at the following variables; they need to match
the structure for your plugin:

```javascript
const pm_name = 'FancyPlugin';
const pm_file = pm_name+'.pm';
const pm_file_path = path.join('Koha', 'Plugin');
```

## Our first example: Adding CSS to our OPAC

As mentioner earlier, one way to build a plugin is by implementing the so-called Koha plugin hooks.
Implementing a **hook** it is just implementing a method, that does what it is expected to do. There's
a list of [implemented plugin hooks](https://wiki.koha-community.org/wiki/Koha_Plugin_Hooks).

If a plugin implements a method named as the plugin hook, it will be called when the Koha code that calls
the hook is invoked. For example, if you want to do something every time a hold is placed, you will implement
an *after_hold_create* method.

In this case, we will go with a simpler example: add some CSS to our OPAC. This is probably overkill as there are
system preferences that can do this already. But in some cases in can be handy (e.g. if you are adding style for
something your plugin is adding to the UI, you don't want to do it manually or adjust it when upgrading
the plugin, right?).

To do this, we are going to use the **opac_head** hook:

```perl
sub opac_head {
    my ( $self ) = @_;

    return q{
  <style>
    body {background-color: pink;}
  </style>
}
}
```
